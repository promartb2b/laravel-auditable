<?php

namespace Fabriordonez\Auditable;

use Illuminate\Database\Eloquent\Model;

class AuditableWithDeletesTraitObserver
{
    use DefaultAuditableId;

    /**
     * Model's deleting event hook
     *
     * @param Model $model
     */
    public function deleting(Model $model)
    {
        $deletedBy = $model->getDeletedByColumn();

        $model->$deletedBy = $this->getAuthenticatedUserId();
        $model->save();
    }

    /**
     * Get authenticated user id depending on model's auth guard.
     *
     * @return int
     */
    protected function getAuthenticatedUserId()
    {
        return auth()->check() ? auth()->id() : $this->getAuthenticatedUserId();
    }

    /**
     * Model's restoring event hook
     *
     * @param Model $model
     */
    public function restoring(Model $model)
    {
        $deletedBy = $model->getDeletedByColumn();

        $model->$deletedBy = null;
    }
}
