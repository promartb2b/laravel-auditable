<?php

namespace Fabriordonez\Auditable;

trait DefaultAuditableId
{
    protected function getDefaultAuditableId()
    {
        if (!property_exists($this, 'defaultAuditableId')) {
            $this->defaultAuditableId = 1;
        }

        return $this->defaultAuditableId;
    }
}
